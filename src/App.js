import React from 'react';
import { v4 as uuid } from 'uuid';
import { Switch, Route } from 'react-router-dom';

import list from './info.json'

import Navbar from './components/Navbar';
import MainList from './components/MainList';
import CompletedTask from './components/CompletedTask';
import Favorites from './components/Favorites';
import './App.scss';

//const id = uuid();

class App extends React.Component {
  state = {
    todos: list.map((todo) => ({ id: uuid(), name: todo.name, description: todo.description})),
    completed: [],
    filter: '',
    favorites: [],
    isFav:false,
  };



  handleAddTaskToList = (newTask) => {
    console.log(newTask)
    this.setState((prevState) => ({
      todos: [
        ...prevState.todos,
        { id: uuid(), name: newTask.name, description: newTask.description}
      ],
    }),

    );
  };

  handleAddTaskToComplete = (completedTask) => {

    this.setState((prevState) => ({
      todos: prevState.todos.filter((todo) => todo.id !== completedTask.id),
      completed: [...prevState.completed, completedTask],
      
    }));
  };

  handleFilterTodos = (ev) => {
    const value = ev.target.value;

    this.setState({
      filter: value,
    });
  };

  editDescription = (editId, newDescription) => {
    
    this.setState((prevState) => ({
      todos: prevState.todos.map((task) => {

        if (task.id === editId) {
          return { ...task, description: newDescription };
        } else {
          return task;
        }
      })
    }));
  };

  handleAddFav= (addFav)=> {
    this.toggleForHeartFav();
    this.setState((prevState)=>({
      todos: [...prevState.todos],
      favorites: [...prevState.favorites, addFav],
    }))
  }

  toggleForHeartFav = () => {
    this.setState((prevState) => ({
      isFav: !prevState.isFav,
    }));
  };

  render() {

    /* console.log(this.state.todos) */
    //filtra de todos 
    const todosFiltered = this.state.todos.filter((todo) => {
      const name = todo.name.toLowerCase().trim(); //toLowerCase para buscar todo en minusculas
      const filter = this.state.filter.toLowerCase().trim();// trim para poner espacios y aun asi filtre

      return name.includes(filter);
    })

    return (
      <section className="App">
        <Navbar
          filter={this.state.filter}
          handleFilterTodos={this.handleFilterTodos}
        />

        <Switch>
          <Route exact path="/" component={(props) => (
            <MainList {...props}
              todos={todosFiltered}
              handleAddTaskToList={this.handleAddTaskToList}
              handleAddTaskToComplete={this.handleAddTaskToComplete}
              editDescription={this.editDescription}
              handleAddFav={this.handleAddFav}
              isFav={this.state.isFav}


            />
          )} />

          <Route exact path="/completedTask" render={() => <CompletedTask
            completed={this.state.completed} />} />
          <Route exact path="/favorites" render={() => <Favorites
            favorites= {this.state.favorites}
          />} />
        </Switch>

      </section>

    );
  }
}

export default App;
