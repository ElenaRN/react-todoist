import React, { Component } from 'react'

import '../styles/completedTask.scss';

export default class CompletedTask extends Component {
    render() {
        return (
            <div className="completedTask">
               
                <div className="completedTask__container">
                 <h2>Tareas Completadas</h2>
                    {this.props.completed.map((complete) => {
                        return (
                            <ul className="completedTask__container__list--style" key={complete.id + complete.name}>
                                <li className="completedTask__container__list--left">
                                    <p className="completedTask__container__list--name">{complete.name}  ✔</p>
                                    <p className="completedTask__container__list--description">{complete.description}</p>
                                </li>
                            </ul>
                        )
                    })}
                </div>

            </div>
        )
    }
}
