import React, { Component } from 'react'

export default class Favorites extends Component {
    render() {
        return (
            <div className="favorites">
                <h2>Tareas favoritas</h2>

                <div className="completedTask__container">
                 
                    {this.props.favorites.map((fav) => {
                        
                        
                            return (
                            <ul className="completedTask__container__list--style" key={fav.id + fav.name}>
                                <li className="completedTask__container__list--left">
                                    <p className="completedTask__container__list--name">{fav.name}</p>
                                    <p className="completedTask__container__list--description">{fav.description}</p>
                                </li>
                            </ul>
                        )
                        
                        
                    })}
                </div>
                
            </div>
        )
    }
}
