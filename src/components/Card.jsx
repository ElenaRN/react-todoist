import React, { Component } from 'react'


import '../styles/card.scss';


export default class Card extends Component {

    state= {    
    error: null,
    values: {
        
        name: '',
        description:'',
       
    }
}

    handleOnSubmit = async (ev) => {
        ev.preventDefault();

        const formValues = this.state.values;

        const objValues = Object.values(formValues);

        const validInputValues = objValues.filter(value => Boolean(value))

        const isValid = objValues.length === validInputValues.length;

        if (isValid) {

            this.props.handleAddTaskToList(formValues);

            this.setState(formValues); //aqui seteamos el form

        } else {
            this.setState({
                error: 'Todos los campos son requeridos'
            })
        }
    };
    

    handleOnAddChange = (ev) => {
        const name = ev.target.name;
        const value = ev.target.value;

        this.setState((prevState) => ({
            values: {
                ...prevState.values,
                [name]: value,
            },
        }));
    };

    render() {
        
        return (
            <div className="Card">
                <form className="Card__form" onSubmit={this.handleOnSubmit}>
                    <label className="Card__form--label" htmlFor="name">
                        <input className="Card__form--input" name="name" value={this.state.values.name} onChange={this.handleOnAddChange} placeholder="Añadir tarea" type="text"></input>
                    </label>

                    <label className="Card__form--label" htmlFor="description">
                        <textarea className="Card__form--input" name="description" value={this.state.values.description} onChange={this.handleOnAddChange} placeholder="Description" type="text"></textarea>
                    </label>
                    
                    <div className="Card__form__container">
                        <button className="Card__form__container--btn">Añadir</button>
                        <button  className="Card__form__container--btn" onClick={this.props.toggleShowInputToSearch} type="button">Cancelar</button>
                    </div>
                </form>
            </div>
        )
    }
}
