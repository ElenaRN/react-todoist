import React from 'react';
import { Link } from 'react-router-dom';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlus, faHome , faSearch, faHeart} from '@fortawesome/free-solid-svg-icons';
//import { faHeart} from '@fortawesome/free-regular-svg-icons';

import '../styles/navbar.scss';

export default class Navbar extends React.Component {
    render() {
        return (
            <nav className="Navbar">

                <div className="Navbar__container">
                    <div className="Navbar__container-link">
                        <Link className="Navbar__container-link--color" to="/"><FontAwesomeIcon className="Navbar__link--color" icon={faHome} /></Link>
                    </div>

                    <div>
                        <Link className="Navbar__container__link-completed--color" to="/favorites"><FontAwesomeIcon className="Navbar__link--color" icon={faHeart} /></Link>
                    </div>
                    
                    <div className="Navbar__container__link-completed">
                        <Link className="Navbar__container__link-completed--color" to="/completedTask"><FontAwesomeIcon className="Navbar__link--color" icon={faPlus} /></Link>
                    </div>

                    <form className="Navbar__container-form">
                        <span><FontAwesomeIcon className="Navbar__container-form--icon" icon={faSearch} /></span>
                        <label className="Navbar__container-form__label" htmlFor="filter">
                            <input className="Navbar__container-form__input" name="filter" value={this.props.filter} onChange={this.props.handleFilterTodos} type="text" placeholder="Buscar" />
                        </label>
                    </form>
                </div>

            </nav>
        )
    }
}
