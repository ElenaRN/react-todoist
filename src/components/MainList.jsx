import React, { Component } from "react";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faPlus,
  faHeart,
  faPencilAlt,
  faSave,
} from "@fortawesome/free-solid-svg-icons";
import {
    
    faHeart as faHeartRegular,
    
  } from "@fortawesome/free-regular-svg-icons";

import "../styles/mainList.scss";
import Card from "./Card";

export default class MainList extends Component {
  state = {
    error: null,
    showAddTask:false,
    isEditing: false,
    editingId: null,
    description: "",
    
    
  };

  toggleShowInputToSearch = () => {
    this.setState((prevState) => ({
        showAddTask: !prevState.showAddTask,
    }));
  };
  

  handleOnChangeForEdit = (ev) => {
    const name = ev.target.name;
    const value = ev.target.value;

    this.setState((prevState) => ({
      ...prevState,
      [name]: value,
    }));
  };

  handleEditAndSave = (task) => {
    if (this.state.isEditing && this.state.editingId === task.id) {
      this.props.editDescription(task.id, this.state.description);

      this.setState({
        isEditing: false,
        editingId: null,
        description: "",
      });
    } else {
      this.setState({
        isEditing: true,
        editingId: task.id,
        description: task.description,
      });
    }
  };

  render() {
    /* console.log(this.props.todos) */
    return (
      <section className="MainList">
        <div className="MainList__container">
          <h2>Bandeja de entrada</h2>

          <div className="MainList__container__list">
            {this.props.todos.map((task) => {
              return (
                <ul
                  className="MainList__container__list--style"
                  key={task.id + task.name}
                >
                  <li className="MainList__container__list--left">
                    <input
                      className="MainList__container__list--input"
                      onClick={() => this.props.handleAddTaskToComplete(task)}
                      role="button"
                      type="radio"
                    ></input>
                    <div className="MainList__container__list--display">
                        <p className="MainList__container__list--name">
                        {task.name}
                        </p>

                    
                      {this.state.isEditing &&
                      this.state.editingId === task.id ? (
                        <textarea
                          type="text"
                          name="description"
                          value={this.state.description}
                          onChange={this.handleOnChangeForEdit}
                        />
                      ) : (
                        <p className="MainList__container__list--description">
                          {task.description}
                        </p>
                      )}
                    </div>
                    <div className="MainList__container__list--containerBtn">
                    <button
                      className="MainList__container__list--button"
                      type="button"
                      onClick={() => this.handleEditAndSave(task)}
                    >
                      {this.state.isEditing &&
                      this.state.editingId === task.id? (
                        <FontAwesomeIcon className="" icon={faSave} />
                      ) : (
                        <FontAwesomeIcon className="" icon={faPencilAlt} />
                      )}
                    </button>
                                        
                    <button onClick= {()=>this.props.handleAddFav(task)} className="MainList__container__list--button">
                     
                     {this.props.isFav === false && task.id === this.props.todos.id? <FontAwesomeIcon className="" icon={faHeartRegular} />:<FontAwesomeIcon className="" icon={faHeart} />}
                      
                    </button>
                    </div>
                  </li>
                </ul>
              );
            })}
          </div>

          {this.state.showAddTask === false ? (
            <button
              className="MainList__container__list--button-link"
              onClick={this.toggleShowInputToSearch}
            >
              <FontAwesomeIcon
                className="MainList__container__list--button-link"
                icon={faPlus}
              />
              Añadir tarea
            </button>
          ) : (
            <Card
              handleAddTaskToList={this.props.handleAddTaskToList}
              toggleShowInputToSearch={this.toggleShowInputToSearch}
            />
          )}
        </div>
      </section>
    );
  }
}
